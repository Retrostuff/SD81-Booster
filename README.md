# SD81 Booster - SD, sound, voice and memory interface for ZX81

An open hardware, open source project packing many features in a single interface.

![SD81 Booster|800](DOC/img/SD81.jpg)

## FEATURES

- Load and save programs from/to a FAT32 microSD card in .P format, supporting common file and directory operations: list directory, copy/move/rename/delete files, change/create/remove directories...
- Up to 512 KB RAM, using a memory mapper in 8 KB blocks. There's also a simplified mode that allows up to 256 KB RAM, needing very simple paging code.
- The selected mapper page for any given block can be read back.
- Use up to 128 user-defined characters easily. You're no longer restricted to the ROM's 64 characters and their inverses: you can define them all!
- AY sound chip emulation with PLAY command.
- AY VGM player, plays in background (does not support VGMs containing other chips than the AY).
- AY sound effects in background, via a programmable virtual machine
- Speech emulation.
- Minimal changes to the standard ROM. In particular, the ZX Printer and tape routines are preserved intact.
- Possibility to run code in the region between 32K and 48K (IMPORTANT: ONLY ENABLE THIS FEATURE IF YOU UNDERSTAND AND ACCEPT THE RISKS, SEE THE "MC45" SECTION BELOW!)

### What it doesn't do

We want to be open about what the interface doesn't do too. There are some limitations as to what it is capable of. Here's a list of some relevant ones:

- It's not possible to use characters other than those supported by the ZX81 for file names. Also, some characters are reserved and can't be used as file names. For paths, the character `/` is the directory separator; the permitted characters for filenames are `A` to `Z`, `0` to `9`, `.`, `,`, `;`, `$`, `(`, `)`, `=`, `+`, `-`. Inverted characters are treated the same as normal characters. Letters are treated as upper case for saving. Note that ending a file name with a space or a period may make the file unreadable when using the SD card in certain operating systems.
- AY sound chip emulation is not cycle-accurate.
- AY sound chip register access is not compatible with other interfaces.
- The memory mapping I/O port and layout are not compatible with other interfaces either.
- There are no new commands per se, or any renamed commands; most of the new functions are implemented through extensions to the LOAD and SAVE commands.

### Possible compatibility issues

- Devices that replace the internal ROM, such as Chroma81, will not work with this interface.
- The memory area between 8 KB and 16 KB is no longer a mirror of the area between 0 and 8KB.
- The same area is now read/write rather than read-only. It contains the ROM expansion, therefore if a program writes to used areas within that space (either on purpose or accidentally), the expanded commands may crash and a hardware reset will be necessary to restore it.
- The ROM changes 8 bytes with respect to the original, therefore any programs that depend on those bytes being the same as in the original ROM, may fail. We consider this highly unlikely to cause issues, though.
- The memory mapper I/O port may clash with that of other interfaces.
- The general command interface I/O ports might clash with those of other interfaces, despite having been selected as the apparently least likely to clash according to port usage lists (but the lists may be obsolete).

## Documentation

See [MANUAL.md](MANUAL.md) for the usage and technical manual. There's also extra documentation for the MCU commands and sound in the [DOC folder](DOC/)

## MC45

MC45 stands for Machine Code in blocks 4 and 5. By default, the ZX81 hardware only allows running machine code in addresses that are in the range 0-32767 (0000-7FFFh); out of this area, every instruction with an opcode in the range 0 to 63 (00h-3Fh) or in the range 128 to 191 (80h-BFh) will be replaced by a NOP by the internal hardware. It's possible to run some code in that area by restricting it to instructions with opcodes in the opposite range, i.e. the ones in the ranges 40h-7Fh and C0h-FFh, but working around the limitation generally takes more instructions than it would take without.

So, there's an MCU (and corresponding BASIC) command that allows activating a mode which allows *any* code, and not just the mentioned opcodes, to run in the area from 32768 to 49151 (8000-BFFF). It has the drawback that the display file can no longer be in that area, though, therefore BASIC programs larger than 16 Kbytes can't be loaded or written while this mode is active.

IMPORTANT: Note however that this is implemented by short-circuiting a Z80 pin, forcing it to 0. THIS MAY DAMAGE THE Z80 OR THE INTERFACE. If you know that your Z80 is CMOS, you're advised to either make an internal mod in your ZX81, by inserting a 680 ohm resistor between pin 18 of the CPU and the rest of the ZX81's PCB, or just disable the feature altogether by taking off the jumper in the interface. If it is NMOS, the feature will in principle work without the mod, though making the mod anyway is recommended.

## Building the ZX81 ROM

The ROM listing for assembling that is used here is the disassembly work of [Geoff Wearmouth](http://web.archive.org/web/20150815035607/http://www.wearmouth.demon.co.uk/zx81.htm), preserved by [Tomaž Šolc](https://www.tablix.org/~avian/spectrum/rom/), both of whom we're grateful to.

The ROM can be built using either [GNU binutils for Z80](https://github.com/atsidaev/binutils-z80), [Pasmo](https://pasmo.speccy.org/) or [MDL](https://github.com/santiontanon/mdlz80optimizer). Maybe other assemblers will work too.

To build with GNU binutils (package `binutils-z80` in Debian-based systems), use the following commands:

    z80-unknown-coff-as sdmodrom.asm -o sdmodrom.o
    z80-unknown-coff-ld -Ttext 0 sdmodrom.o -o sdmodrom.bin

To build with Pasmo:

    pasmo sdmodrom.asm sdmodrom.bin

To build with MDL, dialects `pasmo` and `macro80` can be used, e.g.:

    java -jar mdl.jar -dialect macro80 -bin sdmodrom.bin sdmodrom.asm
