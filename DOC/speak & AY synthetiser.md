## AY SYNTHETIZER

The AY synthetizer included in the SD81 Booster is a software emulator of the AY-3-8910/12 chip.
It can play up to three voices plus envelopes and noise.
It is also compatible at register level.

### AY Registers supported
| REGISTER | DESCRIPTION | B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0 |
| ------| ----| ----| ----| ----| ----| ----| ----| ----| ----|
| R0 | Channel A Tone Period Fine Tune | B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0 | 
| R1 | Channel A Tone Period Coarse Tune |    |    |    |    | B3 | B2 | B1 | B0 | 
| R2 | Channel B Tone Period Fine Tune | B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0 | 
| R3 | Channel B Tone Period Coarse Tune |    |    |    |    | B3 | B2 | B1 | B0 | 
| R4 | Channel C Tone Period Fine Tune | B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0 | 
| R5 | Channel C Tone Period Coarse Tune |    |    |    |    | B3 | B2 | B1 | B0 | 
| R6 | Noise Period |    |    |    | B4 | B3 | B2 | B1 | B0 | 
| R7 | Enable |    |    | Noise C | Noise B | Noise A | Tone C | Tone B | Tone A | 
| R8 | Channel A Amplitude |    |    |    | Mode | L3 | L2 | L1 | L0 | 
| R9 | Channel B Amplitude |    |    |    | Mode | L3 | L2 | L1 | L0 | 
| R10 | Channel C Amplitude |    |    |    | Mode | L3 | L2 | L1 | L0 | 
| R11 | Envelope Period Fine Tune | B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0 | 
| R12 | Envelope Period Coarse Tune | B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0 | 
| R13 | Envelope Shape/Cycle |    |    |    |    |    | B2 | B1 | B0 | 

### PARAMETERS USED IN A PLAY STRING COMMAND

Here is a brief list of the parameters that can be used in
the string of a PLAY command, together with the values they may
have...

| STRING  | FUNCTION |  
| ------| -----------| 
| A..G  | Specifies the pitch of the note within the current octave range. | 
| <span style="background-color:black; color:white">A</span>..<span style="background-color:black; color:white">G</span> | Specifies the pitch of the note within the current octave range + 1. | 
| N,' ' | Dummy note. | 
| V | Specifies the volume to be used (followed by 0 to 15).|
| - | Specifies that a tied note is to be played. | 
| £ | Specifies that the note which follows must be flattened. | 
| = | Specifies that the note which follows must be sharpened. | 
| O | Specifies the octave number to be used (followed by 0 to 8). | 
| 1...12 | Specifies the length of notes to be used. | 
| & | Specifies that a rest is to be played. | 
| W | Specifies the volume effect is to be used in a string. (see envelope table) | 
| U | Enables the volume effect. | 
| X | Specifies duration of volume effect (followed by 0 to 65535). | 
| T | Specifies tempo of music (followed by 60 to 240). | 
| ( ) | Specifies that enclosed phrase must be repeated. | 
| \: \: | Specifies that enclosed comment is to be skipped over. | 
| H | Specifies that the PLAY command must stop. | 


### ENVELOPE TABLE
![Envelope Table](img/envelopes.png)

## VGM PLAYER
The interface incorporates a [VGM file player](https://vgmrips.net/wiki/VGM_Specification), which is capable of playing these files in the background while our program is running on the ZX81.
Only the opcodes referring to the AY chip are supported, according to the following table:
| CODE  | PARAMS | DESCRIPTION |  
| ------| -----------| -----------|
| 0x61 | nn nn | Wait n samples, n can range from 0 to 65535 (approx 1.49 seconds). Longer pauses than this are represented by multiple wait commands. |
| 0x62 |   | wait 735 samples (60th of a second), a shortcut for 0x61 0xdf 0x02 |
| 0x63 |   | wait 882 samples (50th of a second), a shortcut for 0x61 0x72 0x03 |
| 0xA0 | aa dd | 	AY8910, write value dd to register aa |

## PROGRAMABLE EFECT GENERATOR(PEG)
PEG or Programmable Effect generator is an assembly language specifically designed to generate sound effects in our interface.
Like the VGM player, PEG runs in the background without using the zx81's CPU.
The supported instructions are the following: 

![PEG instructions](img/PEG.png) 

The PEG supports a maximum if three threads running in parallel.


## SPEAK SYNTHETIZER

### HOW TO USE
The speech synthesizer works using the internal commands 22 (binary say) and 23 (say).

In both cases a byte with the length of the string must come first, then a string with the sound codes. 

In the case of command 22 the string consists of an array of bytes, but for the command 23 the string is hexadecimal, in the ZX81 character encoding. 


In BASIC you can use LOAD *SAY "<SOUNDS>" which internally calls command 23.

The sound string consists of a series of bank and/or sound codes.  

At system start, E4 is selected by default. When a bank code is found, all sounds after it will be related to this bank until another bank is specified. 

For instance:

LOAD *SAY "80909192E88D94E98E" 

Will play the following sounds: 

E480,E490,E491,E492,E88D,E894,E98E 

In this example, bank E9 will remain selected, so if another command follows, it will use that bank by default. 

### ADDING OUR OWN SOUNDS
The sounds used by the voice synthesizer are stored in a file called "ALOFONES.DAT" located in the SD folder "/SYS/VOICE". 

This file is made up of the sum of all the samples corresponding to each of the codes. These samples are saved as an uncompressed WAV file format, including the header. 

Specifically, the format used is the uncompressed WAV format, PCM, 11025 Hz, 8-bit mono. You can convert your samples with an audio editor as Audacity.

The samples are all stored in order, starting at bank E4 (80..FF sounds) and ending at bank EF. 

In the same SYS folder are included all the .WAV files that are included in the ALOFONES.DAT file that is supplied with the interface. 

If what we want is to add our own sound sample, the only thing we need is a WAV file with the mentioned format copied to the same folder as the rest of wav files. Then we will use a small python program "alofones.py" that will join all the WAVs in a single file and will generate a start points table called "VOICETABLE.h". 
Just copy that file to the arduino folder and recompile the arduino program.


### SPEAKER SOUNDS

| BANK  | SOUND CODE | SOUND DESCRIPTION | 
| ------| -----------| ------------------| 
| $E4 | $80 | 10MS PAUSE | 
| $E4 | $81 |30MS PAUSE | 
| $E4 | $82 |50MS PAUSE | 
| $E4 | $83 |100MS PAUSE | 
| $E4 | $84 |200MS PAUSE | 
| $E4 | $85 |b[OY] | 
| $E4 | $86 |sk[Y] | 
| $E4 | $87 |[E]nd | 
| $E4 | $88 |[C]OMB | 
| $E4 | $89 |[P]ow | 
| $E4 | $8A |dod[GE] |  
| $E4 | $8B |thi[N] | 
| $E4 | $8C |S[i]T | 
| $E4 | $8D |[T]o | 
| $E4 | $8E |[R]ural | 
| $E4 | $8F |s[U]cceed | 
| $E4 | $90 |[M]ilk | 
| $E4 | $91 |par[T] | 
| $E4 | $92 |[TH]ey | 
| $E4 | $93 |s[EE] | 
| $E4 | $94 |b[EI]ge | 
| $E4 | $95 |coul[D] | 
| $E4 | $96 |t[OO] | 
| $E4 | $97 |[AU]ght | 
| $E4 | $98 |h[O]t | 
| $E4 | $99 |[Y]es (long) | 
| $E4 | $9A |h[A]t | 
| $E4 | $9B |[H]e | 
| $E4 | $9C |[B]usiness (short) | 
| $E4 | $9D |[TH]in | 
| $E4 | $9E |b[OO]k | 
| $E4 | $9F |f[OO]d | 
| $E4 | $A0 |[OU]t | 
| $E4 | $A1 |[D]o | 
| $E4 | $A2 |wi[G] | 
| $E4 | $A3 |[V]est | 
| $E4 | $A4 |[G]ot | 
| $E4 | $A5 |[SH]ip | 
| $E4 | $A6 |a[Z]ure | 
| $E4 | $A7 |b[R]ain | 
| $E4 | $A8 |[F]ood | 
| $E4 | $A9 |s[K]y | 
| $E4 | $AA |[C]an't | 
| $E4 | $AB |[Z]oo | 
| $E4 | $AC |a[NG]chor (anchor) | 
| $E4 | $AD |[L]ake | 
| $E4 | $AE |[W]ool | 
| $E4 | $AF |[R]epair | 
| $E4 | $B0 |[WH]ig | 
| $E4 | $B1 |[Y]es (short) | 
| $E4 | $B2 |[CH]urch | 
| $E4 | $B3 |f[IR] (short) | 
| $E4 | $B4 |f[IR] (long) | 
| $E4 | $B5 |b[EAU] | 
| $E4 | $B6 |[TH]ey | 
| $E4 | $B7 |ve[S]t | 
| $E4 | $B8 |[N]o | 
| $E4 | $B9 |[H]oe | 
| $E4 | $BA |st[ORE] | 
| $E4 | $BB |al[AR]m | 
| $E4 | $BC |cl[EAR] | 
| $E4 | $BD |[G]uest | 
| $E4 | $BE |sadd[EL] (saddle) | 
| $E4 | $BF |[B]usiness (Long) | 
| $E4 | $C0 |"ENEMY" | 
| $E4 | $C1 |"ALL CLEAR" | 
| $E4 | $C2 |"PLEASE" | 
| $E4 | $C3 |"GET OFF" | 
| $E4 | $C4 |"OPEN FIRE" | 
| $E4 | $C5 |"WATCH OUT" | 
| $E4 | $C6 |"MERCY" | 
| $E4 | $C7 |"HIT IT" | 
| $E4 | $C8 |"YOU BLEW IT" | 
| $E4 | $C9 |"DO IT AGAIN" | 
| $E4 | $CA |"INCREDIBLE" | 
| $E4 | $FA |"U.F.O." | 
| $E4 | $FB |"MONSTER!" | 
| $E8 | $80 |"AMAZING" | 
| $E8 | $81 |"THANK YOU" | 
| $E8 | $82 |"YUCK" | 
| $E8 | $83 |"ARG" | 
| $E8 | $84 |"THAT'S EASY" | 
| $E8 | $85 |"ATTENTION" | 
| $E8 | $86 |"DANGER" | 
| $E8 | $87 |"TURKEY" | 
| $E8 | $88 |"ACTION" | 
| $E8 | $89 |"AAAAH" | 
| $E8 | $8A |"GOOD" | 
| $E8 | $8B |"HI" | 
| $E8 | $8C |"HARD" | 
| $E8 | $8D |"RIGHT" | 
| $E8 | $8E |"WRONG" | 
| $E8 | $8F |"ATTACK!" | 
| $E8 | $90 |"GREAT!" | 
| $E8 | $91 |"CLIMB" | 
| $E8 | $92 |"DIVE" | 
| $E8 | $93 |"FIRE" | 
| $E8 | $94 |"HELP" | 
| $E8 | $95 |"HURRY" | 
| $E9 | $80 |"JUMP" | 
| $E9 | $81 |"RUN" | 
| $E9 | $82 |"SQUASH!" | 
| $E9 | $83 |"NOW" | 
| $E9 | $84 |"NO" | 
| $E9 | $85 |"NO!" | 
| $E9 | $86 |"YES" | 
| $E9 | $87 |"SORRY" | 
| $E9 | $88 |"OH DEAR" | 
| $E9 | $89 |"GOT'CHA" | 
| $E9 | $8A |"OUTCH" | 
| $E9 | $8B |"OH NO" | 
| $E9 | $8C |"GO FOR IT" | 
| $E9 | $8D |"DO IT" | 
| $E9 | $8E |"LOOK OUT" | 
| $E9 | $8F |"COME ON!" | 
| $E9 | $90 |WHIP SOUND | 
| $E9 | $91 |TONE E991 | 
| $E9 | $92 |MEDIUM EXPLOSION | 
| $E9 | $93 |BEE SOUND | 
| $E9 | $94 |SMALL EXPLOSION E994 | 
| $E9 | $95 |SMALL EXPLOSION E995 | 
| $E9 | $96 |SMALL EXPLOSION E996 | 
| $EA | $80 |"UP" | 
| $EA | $81 |"DOWN" | 
| $EA | $82 |"ALAS" | 
| $EA | $83 |"GO" | 
| $EA | $84 |"FIGHT" | 
| $EA | $85 |"DODGE" | 
| $EA | $86 |PYEW-THUMP | 
| $EA | $87 |PYEW-PYEW | 
| $EA | $88 |MISSILE | 
| $EA | $89 |LARGE EXPLOSION | 
| $EA | $8A |"HA HA HA HA HA" | 
| $EA | $8B |BRRRREEP | 
| $EA | $8C |GUNSHOT | 
| $EA | $8D |TONE EA8D | 
| $EA | $8E |TONE EA8E | 
| $EA | $8F |RANDOM COMPUTER | 
| $EA | $90-|A7 TONES EA90-EAA7 | 
| $EB | $80 |THESE. | 
| $EB | $81 |THOUGHT. | 
| $EB | $82 |THING. | 
| $EB | $83 |TREE (?) | 
| $EB | $84 |TODAY. | 
| $EB | $85 |TOOK. | 
| $EB | $86 |TOWN. | 
| $EB | $87 |UNTIL. | 
| $EB | $88 |VERY. | 
| $EB | $89 |THINK. | 
| $EB | $8A |THROUGH (?) | 
| $EB | $8B |WISH. | 
| $EB | $8C |WORK. | 
| $EB | $8D |YEAR. | 
| $EB | $8E |WHILE. | 
| $EB | $8F |WHITE. | 
| $EB | $90 |WANTED. | 
| $EB | $91 |WATER. | 
| $EB | $92 |SOMETHING. | 
| $EB | $93 |THAN. | 
| $EB | $94 |ANY. | 
| $EB | $95 |BED. | 
| $EB | $96 |CAME. | 
| $EB | $97 |COLD. | 
| $EB | $98 |DAY. | 
| $EB | $99 |HELP. | 
| $EB | $9A |BEST. | 
| $EB | $9B |LET. | 
| $EB | $9C |GAVE. | 
| $EC | $80 |LOOKED. | 
| $EC | $81 |MANY. | 
| $EC | $82 |LARGE. | 
| $EC | $83 |LONG. | 
| $EC | $84 |MEN. | 
| $EC | $85 |MILK. | 
| $EC | $86 |MOST. | 
| $EC | $87 |MUCH | 
| $EC | $88 |MUST. | 
| $EC | $89 |NEVER. | 
| $EC | $8A |NEXT. | 
| $EC | $8B |ONCE. | 
| $EC | $8C |ONLY. | 
| $EC | $8D |OTHER. | 
| $EC | $8E |PEOPLE. | 
| $EC | $8F |PLACE. | 
| $EC | $90 |PLAYED. | 
| $EC | $91 |PLEASE. | 
| $EC | $92 |PRETTY. | 
| $EC | $93 |MORNING. | 
| $EC | $94 |SAID. | 
| $EC | $95 |SHOULD. | 
| $EC | $96 |SISTER. | 
| $EC | $97 |SNOW. | 
| $EC | $98 |STARTED. | 
| $EC | $99 |SUMMER. | 
| $EC | $9A |SURE. | 
| $EC | $9B |TEACHER. | 
| $ED | $80 |COULD. | 
| $ED | $81 |DADDY. | 
| $ED | $82 |CHILDREN. | 
| $ED | $83 |CLOSE. | 
| $ED | $84 |COMING. | 
| $ED | $85 |COUNTRY. | 
| $ED | $86 |DAYS | 
| $ED | $87 |DOOR. | 
| $ED | $88 |EACH. | 
| $ED | $89 |EVERY. | 
| $ED | $8A |FATHER. | 
| $ED | $8B |FINE. | 
| $ED | $8C |FIRST. | 
| $ED | $8D |FOUND. | 
| $ED | $8E |FRIEND. | 
| $ED | $8F |GETTING. | 
| $ED | $90 |GIRL. | 
| $ED | $91 |GIVE. | 
| $ED | $92 |GLAD. | 
| $ED | $93 |GRADE. | 
| $ED | $94 |HAPPY. | 
| $ED | $95 |HOPE. | 
| $ED | $96 |INTO. | 
| $ED | $97 |JUST. | 
| $ED | $98 |LAST. | 
| $ED | $99 |LETTER. | 
| $ED | $9A |LIVE. | 
| $EE | $80 |BACK. | 
| $EE | $81 |ASKED. | 
| $EE | $82 |"OK" | 
| $EE | $83 |"NOW" | 
| $EE | $84 |"SPELL" | 
| $EE | $85 |ABOUT. | 
| $EE | $86 |AFTER. | 
| $EE | $87 |AGAIN. | 
| $EE | $88 |ALONG. | 
| $EE | $89 |ALSO. | 
| $EE | $8A |ANOTHER. | 
| $EE | $8B |AROUND. | 
| $EE | $8C |AS. | 
| $EE | $8D |AWAY | 
| $EE | $8E |BECAUSE. | 
| $EE | $8F |BEFORE. | 
| $EE | $90 |BETTER. | 
| $EE | $91 |BLACK. | 
| $EE | $92 |BOOK. | 
| $EE | $93 |BOYS. | 
| $EE | $94 |BRING. | 
| $EE | $95 |BROTHER. | 
| $EE | $96 |CALLED. | 
| $EE | $97 |THIS IS THE SD81 BOOSTER INTERFACE (FEMALE)|
| $EE | $98 |THIS IS THE SD81 BOOSTER INTERFACE (MALE)|
  