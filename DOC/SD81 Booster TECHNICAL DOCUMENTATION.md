# ZX81 SDX TECHNICAL DOCUMENTATION

## COMMAND DESCRIPTION

### NOP (00h)
 
No operation

Z80 `->` SD81

00h

Z80 `<-` SD81

nothing


### VER (01h)

Get firmware version
Z80 `->` SD81

01h

Z80 `<-` SD81

< x > = (firmware version in BCD format - 1 byte)

Major Version =  x / 16

Minor version = x % 16


### PWD (02h)

Get current directory

Z80 `->` SD81

02h 

Starts working directory string. After this command will start the string transfer sequence.

String transfer consists of a sequence of �get_next_char� commands. String will be terminated with a character 255.

A non �get_next_char� command will terminate the sequence.


### CD (03h)

Change directory

Z80 `->` SD81

03h, < dir name (pascal string) >

Z80 `<-` SD81

< error code >


### DEL (04h)

Delete file

Z80 `->` SD81

04h, < dir name (pascal string) >  

Z80 `<-` SD81

< error code >


### MD (05h)

Make directory

Z80 `->` SD81

05h, < dir name (pascal string) >  

Z80 `<-` SD81

< error code >


### RD (06h)

Remove directory

Z80 `->` SD81

06h, < dir name (pascal string) >  

Z80 `<-` SD81

< error code >


### MOVE (07h)

Move file

Z80 `->` SD81

07h, < current name (pascal string) > , < new name (pascal string) >  

Z80 `<-` SD81

< error code >

### COPY (08h)

Copy file

Z80 `->` SD81

08h, < source name (pascal string) > , < destination name (pascal string) >

Z80 `<-` SD81

< error code >

### LOAD (09h)

Load File. 

Z80 `->` SD81

09h, < len (1 byte) >, < file name (string[len]) >

Z80 `<-` SD81

< low byte of file len > < high byte of file len > < bytes >

< error code >


### SAVE (0Ah)

Save File

Z80 `->` SD81

0Ah, < len (1 byte) >, < file name (string[len]) > < low byte file len > < high byte file len > < bytes >

Z80 `<-` SD81

< error code >


### DIR OPEN(0Ch)

Starts a directory listing operation

Z80 `->` SD81

0Ch, < dir+wildcards (pascal string)(1) > 

Z80 `<-` SD81

< error code >

Starts getting directory string. After this command will start the string transfer sequence.

String transfer consists of a sequence of �get_next_char� commands. String will be terminated with a character 255.

A non �get_next_char� command will terminate the sequence inmediatly.


### GET NEXT CHAR (0Dh)

Get next char within a command with screen output

Z80 `->` SD81

0Dh

Z80 `<-` SD81

< next char >

FFh means string end

After a FFh a status byte is also sent


### FREE TXT (0Eh)

Show free SD space (text format)

Z80 `->` SD81

0Eh

Z80 `<-` SD81

Starts getting free SD space string. After this command will start the string transfer sequence.

String transfer consists of a sequence of �get_next_char� commands. String will be terminated with a character 255.

A non �get_next_char� command will terminate the sequence.



### FREE (0Fh)

returns free SD space

Z80 `->` SD81

0Fh

Z80 `<-` SD81

< SD size bits 7-0 > < SD size bits 15-8 > < SD size bits 23-16 > < SD size bits 31-24 > 

< free space bits 7-0 > < free space bits 15-8 > < free space bits 23-16 > < free space bits 31-24 > 



### OPENDIR2(10h)

Starts directory operation to be used together getrow & getrowlen

Z80 `->` SD81

10h, < dir+wildcards (pascal string)(1) >

Z80 `<-` SD81

< error code >



### GETROWLEN(11h)

returns the lenght of the specified directory row

Z80 `->` SD81

11h, < row low > < row high >

Z80 `<-` SD81

< len >

< error code >



### GETROW(12h)

returns the string of the specified directory row

Z80 `->` SD81

12h, < row low > < row high >

Z80 `<-` SD81

< filename (Pascal String) >

< error code > ????????

### ENABLE_M1NOT (13h)

Enable the external M1NOT mod

Z80 `->` SD81

13h

Z80 `<-` SD81

nothing


### DISABLE_M1NOT (14h)

Disable the external M1NOT mod

Z80 `->` SD81

14h

Z80 `<-` SD81

nothing


### BINARY SAY (16h)

send a string of bytes to the speech synthetizer

Z80 `->` SD81

17h< len > < array of phonemes >

Z80 `<-` SD81

< error code >

### SAY (17h)

send a hex string to the speech synthetizer

Z80 `->` SD81

17h< len > < array of phonemes >

Z80 `<-` SD81

< error code >



### AY SET REG (18h)

Write a value to an AY register

Z80 `->` SD81

18h< reg > < value >

Z80 `<-` SD81

< error code >



### AY GET REG (19h)

Returns the value of an AY register

Z80 `->` SD81

19h< reg >

Z80 `<-` SD81

< value >

< error code >



### PLAY (1Ah)

Play a music string

Z80 `->` SD81

1Ah< len > < array of notes >

Z80 `<-` SD81

< error code >



### SET 128CHARS (1Bh)

Select the 128 chars user define chars mode

Z80 `->` SD81

1Bh

Z80 `<-` SD81

nothing



### SET 64CHARS (1Ch)

Select the 64 chars user define chars mode

Z80 `->` SD81

1Ch

Z80 `<-` SD81

nothing



### SET FULLPAGING (1Dh)


Z80 `->` SD81

1Dh

Z80 `<-` SD81

nothing



### HALFPAGING (1Eh)

Z80 `->` SD81

1Eh

Z80 `<-` SD81

nothing



### GETBYTE (20h)

Read a byte from a register

Z80 `->` SD81

20h< reg >

0..127 volatile registers

128..255 persistent registers

Z80 `<-` SD81

< value >



### SETBYTE (21h)

Write a byte to a register

Z80 `->` SD81

21h< reg > < value >

0..127 volatile registers

Z80 `<-` SD81

nothing


128..255 persistent register

128..255 persistent registers


### PLAY_VGM (22h)

Play a VGM music file. Only AY format is supported.

Z80 `->` SD81

22h< len (1 byte) >, < file name (string[len]) >


Z80 `<-` SD81

< error code >





### STOP_VGM (23h)

Stop the current VGM music file

Z80 `->` SD81

23h

Z80 `<-` SD81

nothing


### PAUSE_VGM (24h)

Pause the current VGM music file

Z80 `->` SD81

24h

Z80 `<-` SD81

nothing


### CONT_VGM (25h)

Continue the current VGM music file after a pause

Z80 `->` SD81

25h

Z80 `<-` SD81

nothing

### LOOP_VGM (26h)

Set playing mode to loop for VGM music

Z80 `->` SD81

26h

Z80 `<-` SD81

nothing



### LOAD_PEG (28h)

Load machine code in PEG format to the specified address in the PEG memory.

Z80 `->` SD81

28h< addr > < len > < PEG string >

Z80 `<-` SD81

nothing


### PLAY_PEG (29h)

Start a task calling to the PEG code stored at the specified address using the specified thread 

Z80 `->` SD81 

29h,< thread > < addr > 

Z80 `<-` SD81 

nothing 
 

### STOP_PEG (2ah)

Stops the specified PEG thread

Z80 `->` SD81

2ah< thread > 

Z80 `<-` SD81

nothing



### PAUSE_PEG (2bh)

Pause the specified PEG thread

Z80 `->` SD81

2bh< thread > 

Z80 `<-` SD81

nothing


### CONT_PEG (2ch)

Continue a previously paused thread

Z80 `->` SD81

2ch< thread > 

Z80 `<-` SD81

nothing

### SDLOAD_PEG (2dh)

Load a PEB file from the SD

Z80 `->` SD81

2dh,< len (1 byte) >, < file name (string[len]) >, < addr (1 byte) >

Z80 `<-` SD81

< error code >



### NOTES

(1)	Pascal string format: < len 1 byte > < char array up 255 >



### ERROR MESSAGES



0 - OK

C - Command not recognized

G (1) - file or dir doesn�t exist

H (2) - Not a dir

I (3) - generic error

J (4) - destination file already exist

K(5) - File too large

L(6) - Can�t create destination file

M(7) - Writing error

N(8) - reading error

O(9) - Out of Range in a PLAY command

P(10) - Invalid note name

Q(11) - Invalid file format

R(12) - File not opened

S(13) - Forbiden operation



## MCU SOFTWARE

The microcontroller software is made using the Arduino IDE, so we will need this environment to be able to carry out any modification or customization of this software.

The Arduino IDE is available for Windows, Linux and macOS, on the official Arduino page:
[https://www.arduino.cc/en/software](https://www.arduino.cc/en/software)

Once the environment has been downloaded and installed we must select as Target �Arduino Mega or Mega 2560�:

![Target board](img/arduino001.png)


## LIBRARY DEPENDENCES

In order to compile the software for this project, it will be necessary to have Bill Greiman's SDFat 2.2.0 library installed in the Arduino IDE.
To do this we will select `Tools/manage libraries�.` and then we will install the SDFat library.

![SDFat library](img/arduino002.png)

Additionally, it is also necessary to have the SDJTAG library installed that manages the writing of the CPLD. This library can be found in the project repository and consists of a modification of Marcelo Jimenez's JTAG library, modified so that it reads from the interface's microSD instead from the serial port.

To install this last library, we must copy the SDJTAG folder from the repository to the Arduino lib folder, usually located in `Documents/Arduino/lib`.


## UPDATING THE FIRMWARE

Updating the SD81 Booster with a new firmware version is as simple as connecting it with a micro-USB cable to a PC and executing a simple command.

### WINDOWS
Remove the SD81 Booster from the console and connect the Arduino micro-USB port to the PC, the Arduino driver should install and appear as a new CH340 COM port in the system.

The number of the COM port needs to be known and this can be determined by looking in the "Bluetooth and other devices" window of the Control Panel like this:

![Serial Ports](img/arduino003.png)
If there are problems finding the correct number of the port then there is a utility called "LISTCOM.EXE" included in the repository that will list all the COM ports available on the PC, test each of these to find the correct port.

Once the port is known proceed to run the flashing utility `FLASH.BAT`, having already copied the required `.HEX` firmware file to the same directory as the utility.

The syntax of the utility is as follows:

FLASH `[COMPORT] < file >`

For example, to load the file `update.HEX` through the COM7 port, execute the following command on the PC:
` FLASH COM7 update.HEX`
*Note:* DO NOT INTERRUPT THE FLASHING PROCEDURE - Wait for the flashing operation to finish, after which the *SD81 Booster* will automatically reset.

### Linux

Install avrdude with the command
sudo apt install avrdude

Try to uninstall brltty from the system, because it interferes with the CH340 chip driver.
sudo apt-get remove --auto-remove --purge brltty

Remove the SD81 Booster from the console and connect the Arduino micro-USB port to the PC, the Arduino driver should install and appear as a new CH340 COM port in the system.
ls -l /dev/ttyUSB0

Use the following command to update the firmware, replacing <filename> with the name of the firmware file:
avrdude -patmega2560 -cwiring -P/dev/ttyUSB0 -b115200 -D -Uflash:w:SD81BoosterV2.020.hex:i

Wait until the operation is completed.

## PROGRAMMING THE CPLD

To program the CPLD we will need to have soldered at least the CPLD, the Arduino, the microSD reader, and the resistors from R4 to R10.

Once this is done, we will copy the file `SD81CPLD.xsv` to the root folder of the microSD and we will power up the interface (either by connecting it to the zx81 itself or by connecting the USB cable from the Arduino)

At startup, the Arduino will detect the file and the status LED will flash between blue and red for 5 seconds, warning that the recording process is about to begin (do not cut off the power during the process that follows).

After 5 seconds, the status led will begin to flash red, which will turn green as the recording progresses.

Once the recording is finished, the status LED will remain solid green if everything has gone right, and solid red if an error has occurred.

The `SD81CPLD.xsvf` file will be renamed to `SD81CPLD.done` in case of successful recording and �SD81CPLD.error� in case of any error. if, for any reason, the process is interrupted the file will be renamed as `SD81CPLD.process`

As an alternative, we can do it with a programming cable (Xilinx USB Platform Cable) and the Impact software that is part of the ISE Design suite and can be downloaded for free from the Xilinx website.

The software is available for Windows 7, and Linux, although a version for Windows 10 and 11 is also available that consists of a Linux virtual machine for VirtualBox.

[XILINX download site](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive-ise.html)

The original Xilinx Platform Cable has a high price, but we can buy a very cheap clone (less than �30) by doing a simple search on AliExpress or eBay.

![XILINX Cable](img/arduino004.png)

To program the CPLD we should connect the following cables to the connector located at the bottom right of the interface board, in the following order counting from top to bottom:

-   TMS

-   TDI

-   TDO

-   TCK

-   GND

-   VREF

In addition to the above, we must have the board powered, for example, by connecting the USB connector of the Arduino, and we will connect the USB of the programmer to the PC.

Once we have everything connected, we will load the Impact program.

The IMPACT main window is shown below.

![IMPACT01](img/arduino005.png)

Double click on the Boundary Scan option. And then in the Boundary Scan window, press the right mouse button and select Initialize Chain.

![IMPACT02](img/arduino006.png)

If everything goes well, our CPLD `(XC95144XL)` should appear.

![IMPACT03](img/arduino007.png)

Select the CPLD by clicking on it; then press the right mouse button once and select `Assign New Configuration File`. Browse until you find the CPLD configuration file `(SD81.JED)`, select it and press Open. If the process runs successfully the name of the .jed file should appear below the CPLD.

Press the right button on the CPLD again and now select �Erase�. Once the deletion is finished, repeat the process and select �Program�.

If everything went correctly, �PROGRAM SUCCEEDED� will appear.

## STATUS LED

| STATUS LED COLOR | DESCRIPTION |  
| ------| -----------|
| BLUE/RED Fast blinking | SD init failed or file access error | 
| ORANGE/RED Fast blinking | Mem Access failed | 
| YELLOW/PINK Fast blinking | CPLD prog starting procedure | 
| RED to GREEN blinking ramp | CPLD is being programming (fixed GREEN means prog is finished)|
| GREEN fixed | OK | 
| CYAN | executing a command |
| GREEN/CYAN blinking | background task running | 
