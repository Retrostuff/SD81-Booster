 |  Game |  Result |  mode |  NOTES  | 
 | ------| --------|  -----| --------| 
 |  ZX80 PACMAN (Paul Farrow) |  ok |  single file |   | 
 |  HR BLACKJACK |  ok |  single file |  
 |  Common Emiter (Syncware) |  ok |  single file |  
 |  POKER HR |  ok |  single file |  First LOAD *128C  | 
 |  Quick Silva Invaders |  ok |  single file |  First LOAD *128C  | 
 |  HR PICTURES |  ok |  single file |    | 
 |  PANICOHR |  ok |  multifile |  First LOAD FAST  | 
 |  HRCHESS |  ok |  multifile |  First LOAD FAST  | 
 |  WATOR (SYNCWARE) |  ok |  single file |    | 
 |  VDAQ1-HR |  ok |  single file |    | 
 |  SPIRO |  ok |  single file |    | 
 |  SNORTH |  ok? |  multifile |  First LOAD FAST  | 
 |  SHREB |  ok |  multifile |  First LOAD FAST+LOAD *128C  | 
 |  NIM2K |  ok |  single file |    | 
 |  FSCAPES |  not ok |  multifile |  con LOAD *128C se ve algo pero al final no funciona  | 
 |  FONTKIT |  ok |  multifile |  First LOAD FAST+LOAD *128C  | 
 |  Dungeon of Ymir  |  not ok |  multifile |  https://www.timexsinclair.com/computer_media/dungeon-of-ymir/  | 
 |  Against the Element (Paul Farrow) |  ok  |  single file |  First POKE 4096,1 http://www.fruitcake.plus.com/Sinclair/ZX81/NewSoftware/  | 
 |  PACMAN (Lightning Software) |  ok |  single file |  http://www.fruitcake.plus.com/Sinclair/ZX81/NewSoftware/  | 
 |  KONG ZX80 |  ok |  single file |  http://www.fruitcake.plus.com/Sinclair/ZX81/NewSoftware/  | 
 |  Nanako in Classic Japanese Monster Castle 81 |  ok  |  single file |  First POKE 4096,1 https://www.mojontwins.com/juegos_mojonos/nanako-in-classic-japanese-monster-castle-81/  | 
 |  25th Annyversary demo |  ok |  single file |  https://spectrumcomputing.co.uk/entry/31996/ZX81/25thanni  | 
 |  1KCHESS |  ok |  single file |    | 
 |  BOOSTER (SOFTFARM) |  ok |  single file |  First POKE 4096,1 http://www.pictureviewerpro.com/hosting/zx81/softwarefarm.htm  | 
 |  FORTY NINER |  ok |  single file |  First POKE 4096,1  http://www.pictureviewerpro.com/hosting/zx81/softwarefarm.htm  | 
 |  ROCKET MAN |  ok |  single file |  First POKE 4096,1 http://www.pictureviewerpro.com/hosting/zx81/softwarefarm.htm  | 
 |  Z-XTRICATOR |  ok |  single file |  First POKE 4096,1 http://www.pictureviewerpro.com/hosting/zx81/softwarefarm.htm  | 
 |  MANIC MINER |  ok  |  single file |    | 
 |  1KREVERSE |  ok |  single file |    | 
 |  MINI DEMO 1K WRK16  |  ok |  single file |  Some flicking  | 
 |  3D MAZE |  ok |  single file |    | 
 |  AEROPORTO HR |  ok |  single file |  First LOAD *128C  | 
 |  ALIENDES |  ok |  single file |    | 
 |  ALLCHG.P |  ok |  single file |    | 
 |  ASTEROID |  ok |  single file |    | 
 |  MAZOGS |  ok |  single file |    | 
 |  BREAKOUT |  ok |  single file |    | 
 |  ZX-GALAXIANS |  ok |  single file |  First LOAD *128C  | 
 |  SHREB DEMO |  ok |  single file |    | 
 |  HRG-MS VERSION 2.7 |  ok |  single file |    | 
 |  SPACE INVADERS |  ok |  single file |    | 
 |  HERO-HR | ok |  single file |    | 
 |  SUPER REFRIED GUN OPERATION ’81 |  ok |  single file |  First POKE 4096,1 https://www.mojontwins.com/juegos_mojonos/super-refried-gun-operation-81/  | 
 |  UWOL 81 |  ok |  single file |    | 
 |  CARTOONS |  ok |  single file |    | 
 |  WRX1K v1.0 |  ok |  single file |    | 
  | 