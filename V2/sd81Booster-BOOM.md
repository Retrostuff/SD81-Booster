# Partlist

| Part      | Value              | Device             | Package                   |
| ----| ----| ---| ----|
| C1,C2,C3,C8,C9,C12 | 100nF                | Ceramic capacitor    | C025-040X050             |   
| C4             | 470nF                | Ceramicapacitor      | C025-040X050             |   
| C7             | 10uF                 | Polar capacitor      | Radial                   |   
| C10            | 250uF                | Polar capacitor      | Radial                   |   
| C11            | 0.05uF               | Ceramic capacitor    | C025-040X050             |   
| IC1            | AS6C4008-55PIN       | AS6C4008-55PIN       | DIL32                    |   
| IC2            | REG1117              | Volt. regul. 5V-3V3  | SOT223                   |   
| IC3            | 74LS125N             | 74LS125N             | DIL14                    |   
| IC4            | XC95144XL-10TQG100C  | XC95144XL-10TQG100C  | TQFP100                  |   
| IC5,IC8,IC9    | 74HC573N             | 74HC573N             | DIL20                    |   
| IC7            | LM386N-4             | LM386N-4             | DIL08                    |   
| J1             | AUDIO_JACK_3.5MM_KIT | AUDIO_JACK_3.5MM_KIT | AUDIO-JACK-KIT           |   
| LED1           | SD                   | LED5MM               | LED5MM                   |   
| LED2           | STATUS               | LED-RGB-CCDIFFUSE    | LED-RGB-THRU             |   
| R1             | 10K                  | POT                  | POT-WHEEL                |   
| R2             | 150                  | R-EU_0204/7          | 0204/7                   |   
| R3             | 68                   | R-EU_0204/7          | 0204/7                   |   
| R4,R5,R7       | 180                  | R-EU_0204/7          | 0204/7                   |   
| R6             | 100                  | R-EU_0204/7          | 0204/7                   |   
| R8,R9,R10      | 330                  | R-EU_0204/7          | 0204/7                   |   
| R11            | 680                  | R-EU_0204/7          | 0204/7                   |   
| R12            | 100                  | R-EU_0204/7          | 0204/7                   |   
| R13            | 150                  | R-EU_0204/7          | 0204/7                   |
| R16            | 10                   | R-EU_0204/7          | 0204/7                   |  
| R14            | 68K                  |  R-EU_0204/7         | 0204/7                   |
| S1             | ARDUINO RESET        | PUSH BUTTON          | B3F-10XX                 |   
| SP1            | 32OHM                | SPEAKER              | ALT                      |   
| U$2            |                      | ARDUINO_MEGA2560_PRO |                          |   
| U$3            |                      | MICRO-SD-MODULE      |                          |   
